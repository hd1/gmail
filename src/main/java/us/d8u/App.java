package us.d8u;

import java.io.PrintWriter;
import java.util.Properties;
import javax.mail.AuthenticationFailedException;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.internet.InternetAddress;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;
import org.apache.log4j.Level;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.ISOPeriodFormat;

public class App
{
  private static Logger logger = Logger.getLogger("us.d8u.App");

  private static boolean isValid(Message msg) throws MessagingException {
    DateTime received = new DateTime(msg.getReceivedDate());
    InternetAddress from = (InternetAddress)msg.getFrom()[0];
    if (received.plusDays(30).isAfterNow() == false) return false;
    //if (from.getAddress().equalsIgnoreCase(emailAddress)) return false;
    if (msg.getFolder().getName().contains("Sent")) return false;
    return true;
  }

  public static void ProcessOptions(String[] args) throws ParseException {
    Options options = new Options();
    options.addOption("u", "user", true, "Gmail username");
    options.addOption("p", "password", true, "Gmail passsword");
    options.addOption("v", "verbose", false, "Be verbose");
    CommandLineParser parser = new GnuParser();
    CommandLine line = parser.parse(options, args);
    if (line.hasOption("user")) {
      System.setProperty("us.d8u.user", line.getOptionValue("user"));
    }
    if (line.hasOption("password")) {
      System.setProperty("us.d8u.password", line.getOptionValue("password"));
    }
  }
  static int msgCount = 0;
  static Store store = null;
  public static void cleanFolder(String folderName, int dayCount) throws MessagingException {
    Folder folder = store.getFolder(folderName);
    folder.open(Folder.READ_WRITE);
    for (Message msg : folder.getMessages()) {
      DateTime date = new DateTime(msg.getReceivedDate().getTime());
      DateTime twoWeeksLater = date.plusDays(dayCount);
      if (twoWeeksLater.isBefore(DateTime.now())) {
        msg.setFlag(Flag.DELETED, true);
        msgCount++;
      }
    }
    folder.close(true);
  }

  public static void main( String[] argv ) {
    DateTime start = new DateTime();
    Options options = new Options();
    options.addOption("u", "user", true, "Gmail username");
    options.addOption("p", "password", true, "Gmail passsword");
    try {
      ProcessOptions(argv);
    } catch (Throwable e) {
      HelpFormatter formatter = new HelpFormatter();
      formatter.printHelp("us.d8u.App", options);
      System.exit(e.hashCode());
    }
    Properties props = System.getProperties();
    props.setProperty("mail.store.protocol", "imaps");
    Session session = Session.getDefaultInstance(props, null);
    try {
      store = session.getStore("imaps");
    } catch (NoSuchProviderException e) {
      logger.fatal(e.getMessage(), e);
      System.exit(-1);
    }

    try {
      store.connect("imap.gmail.com", System.getProperty("us.d8u.user"), System.getProperty("us.d8u.password"));
      logger.debug("Connected to Gmail!");
    } catch (AuthenticationFailedException e) {
      logger.fatal(e.getMessage(), e);
      System.exit(-1);
    } catch (MessagingException e) {
      logger.fatal(e.getMessage(), e);
      System.exit(-1);
    }

    // see which folders are available
    Folder folder = null;

    try {
      folder = store.getFolder("INBOX");
      logger.debug("Folder obtained!");
    } catch (MessagingException e) {
      logger.fatal(e.getMessage(), e);
      System.exit(-1);
    }

    try {
      folder.open(Folder.READ_WRITE);
      logger.debug("Folder opened!");
    } catch (MessagingException e) {
      logger.fatal(e.getMessage(), e);
      System.exit(-1);
    }

    try {
      for (Message msg : folder.getMessages()) {
        if (!isValid(msg)) {
          msg.setFlag(Flag.DELETED, true);
          msgCount++;
          logger.debug("set deleted flag for message sent on "+msg.getReceivedDate().toString());
        }
      }
      logger.debug("Messages marked as deleted!");
    } catch (MessagingException e) {
      logger.fatal(e.getMessage(), e);
      System.exit(-1);
    }

    try {
      folder.close(true); 
      logger.debug("expunged messages!");
    } catch (MessagingException e) {
      logger.fatal(e.getMessage(), e);
      System.exit(-1);
    }

    try {
      javax.mail.Folder[] folders = store.getDefaultFolder().list("*");
      for (Folder folder2 : folders) {
        if ((folder2.getType() & javax.mail.Folder.HOLDS_MESSAGES) != 0) {
          logger.debug(folder2.getFullName() + ": " + folder2.getMessageCount());
        }
      }
    } catch (MessagingException e) {
      logger.fatal(e.getMessage(), e);
      System.exit(-1);
    }

    try {
      folder = store.getFolder("[Google Mail]/Sent Mail");
      logger.debug("Obtained sent mail folder!");
    } catch (MessagingException e) {
      logger.fatal(e.getMessage(), e);
      System.exit(-1);
    }
    try {
      folder.open(Folder.READ_WRITE);
      logger.debug("Opened Sent-Mail");
    }
    catch (MessagingException e) {
      logger.fatal(e.getMessage(), e);
    }

    try {
      for (Message msg : folder.getMessages()) {
        try {
          if (msg.getSubject().equals("Thought you might be interested")) {
            msg.setFlag(Flag.DELETED, true);
            msgCount++;
            logger.debug("set deleted flag in Sent-Mail");
          }
        } catch (NullPointerException e) { 
          logger.fatal(msg.getMessageNumber() + " -- " + e.getMessage(), e);
        }
      }
      logger.debug("Marked what should be deleted as such in Sent-Mail");
    } catch (MessagingException e) {
      logger.fatal(e.getMessage(), e);
    }

    try {
      folder.close(true);
      logger.debug(String.format("%d messages expunged", msgCount));
    } catch (MessagingException e) {
      logger.fatal(e.getMessage(), e);
    }

    // clean labels
    try {
      cleanFolder("20s-30social", 0);
      cleanFolder("Dean", DateTime.now().dayOfMonth().getMaximumValue());
      cleanFolder("Japanese", DateTime.now().dayOfMonth().getMaximumValue());
      cleanFolder("ERM Reports", 0);
      cleanFolder("IP", 90);
      cleanFolder("Python3000", 90);
      cleanFolder("R", DateTime.now().dayOfMonth().getMaximumValue());
      cleanFolder("Toastmasters", 0);
      cleanFolder("baypiggies", 75);
      cleanFolder("apache", DateTime.now().dayOfMonth().getMaximumValue());
      cleanFolder("bookclub", 0);
      cleanFolder("burningman", 180);
      cleanFolder("chomsky", 0);
      cleanFolder("funcheapsf", 0);
      cleanFolder("jobs", DateTime.now().dayOfMonth().getMaximumValue());
      cleanFolder("jython",DateTime.now().dayOfMonth().getMaximumValue());
      cleanFolder("legal", 60);
      cleanFolder("lucene-user", 0);
      cleanFolder("missingSync", 0);
      cleanFolder("mobilemonday", DateTime.now().dayOfMonth().getMaximumValue());
      cleanFolder("nutch", 0);
      cleanFolder("politech", 0);
    } catch (MessagingException e) {
      logger.fatal(e.getMessage(), e);
    }
    try {
      folder = store.getFolder("[Gmail]/All Mail");
      folder.open(Folder.READ_WRITE);
      for (Message msg : folder.getMessages()) {
        String subject = msg.getSubject();
        String lowercaseSubject = subject.toLowerCase();
        int voteLocation = lowercaseSubject.indexOf("vote");
        logger.debug(voteLocation+" is where vote was found in "+lowercaseSubject);
        if (msg.getSubject().toLowerCase().indexOf("[vote]") != -1) {
          msg.setFlag(Flag.DELETED, true);
        }
      }
    } catch (MessagingException e) {
      logger.fatal(e.getMessage(), e);
    }


    try {
      store.close();
      logger.debug(String.format("Store closed, run completed in %s", ISOPeriodFormat.standard().print(new Period(new DateTime(), start))));
    } catch (MessagingException e) {
      logger.fatal(e.getMessage(), e);
    }
  }
}
